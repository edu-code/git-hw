# git-hw

В практическом задании вы на практике познакомитесь с:

- BFG Repo-Cleaner
- разрешением merge conflict
- ссылками на комит относительно указателя HEAD
- командой git log
- командой cherry-pick
- настройкой хуков

В каждой папке есть файл с моим решенением в формате asciinema. Для просмотра нужно установить asciinema через pip
```
pip3 install asciinema --user
```
Просмотр записи
```
asciinema play filename
```
